## Themes for this milestone :clipboard:

1. Theme
1. Theme

See our [direction](https://about.gitlab.com/direction/manage/foundations/design_system/) page for our long-term plans.

## Quick Reference :reminder_ribbon:

- :calendar: Important dates:
  - This milestone runs from **yyyy-mm-dd - yyyy-mm-dd**
- :reminder_ribbon: Issue Boards
  - [Issues by Workflow label](https://gitlab.com/groups/gitlab-org/-/boards/7550297?milestone_title=Started&label_name%5B%5D=group::design%20system)
  - [Issues by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/7295775?label_name%5B%5D=group::design%20system)
- :gitlab-hero: Want to contribute?
  - See our [issues seeking community contributions](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_asc&state=opened&label_name%5B%5D=group::design%20system&label_name%5B%5D=Seeking%20community%20contributions&first_page_size=100)

## Validation & Design Track :mag_right:

Summary of key goals and plans

- [ ] Issue
- [ ] Issue
- [ ] Issue

## Build Track :tools:

Summary of key goals and plans

- [ ] Issue
- [ ] Issue
- [ ] Issue

## Document Track :memo:

Summary of key goals and plans

- [ ] Issue
- [ ] Issue
- [ ] Issue


## Planned Time Off :palm_tree:

Hey team! :wave: Please add your planned time off below for this release period.

_\*Note, you do not need to update this table if you take unplanned time off, this is meant to be a quick snapshot of capacity for those who don't have access to capacity planning._

| Team Member | Time Off |
|-------------|----------|
| @chrismicek | |
| @danmh | |
| @igloude | |
| @jeldergl |  |
| @jtucker_gl | |
| @leipert | |
| @pgascouvaillancourt | |
| @samdbeckham | |
| @sdejonge | |
| @thaina.t | |
| @vanessaotto |  |

/label ~"group::design system" ~"Planning Issue" ~"type::ignore"